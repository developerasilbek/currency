package uz.devops.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import uz.devops.domain.Language;
import uz.devops.service.dto.LanguageDTO;
import uz.devops.service.dto.RequestDTO;

public interface LanguageService {
    LanguageDTO save(LanguageDTO languageDTO);

    LanguageDTO update(LanguageDTO languageDTO);

    Optional<LanguageDTO> partialUpdate(LanguageDTO languageDTO);

    List<LanguageDTO> findAll();

    Optional<LanguageDTO> findOne(Long id);

    void delete(Long id);

    Map<String, Language> saveAll(List<Language> languageList);
}
