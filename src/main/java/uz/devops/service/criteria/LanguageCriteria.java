package uz.devops.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class LanguageCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter russian;

    private StringFilter uzbek;

    private StringFilter cyrillic;

    private StringFilter english;

    private Boolean distinct;

    public LanguageCriteria() {}

    public LanguageCriteria(LanguageCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.russian = other.russian == null ? null : other.russian.copy();
        this.uzbek = other.uzbek == null ? null : other.uzbek.copy();
        this.cyrillic = other.cyrillic == null ? null : other.cyrillic.copy();
        this.english = other.english == null ? null : other.english.copy();
        this.distinct = other.distinct;
    }

    @Override
    public LanguageCriteria copy() {
        return new LanguageCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getRussian() {
        return russian;
    }

    public StringFilter russian() {
        if (russian == null) {
            russian = new StringFilter();
        }
        return russian;
    }

    public void setRussian(StringFilter russian) {
        this.russian = russian;
    }

    public StringFilter getUzbek() {
        return uzbek;
    }

    public StringFilter uzbek() {
        if (uzbek == null) {
            uzbek = new StringFilter();
        }
        return uzbek;
    }

    public void setUzbek(StringFilter uzbek) {
        this.uzbek = uzbek;
    }

    public StringFilter getCyrillic() {
        return cyrillic;
    }

    public StringFilter cyrillic() {
        if (cyrillic == null) {
            cyrillic = new StringFilter();
        }
        return cyrillic;
    }

    public void setCyrillic(StringFilter cyrillic) {
        this.cyrillic = cyrillic;
    }

    public StringFilter getEnglish() {
        return english;
    }

    public StringFilter english() {
        if (english == null) {
            english = new StringFilter();
        }
        return english;
    }

    public void setEnglish(StringFilter english) {
        this.english = english;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LanguageCriteria that = (LanguageCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(russian, that.russian) &&
            Objects.equals(uzbek, that.uzbek) &&
            Objects.equals(cyrillic, that.cyrillic) &&
            Objects.equals(english, that.english) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, russian, uzbek, cyrillic, english, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LanguageCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (russian != null ? "russian=" + russian + ", " : "") +
            (uzbek != null ? "uzbek=" + uzbek + ", " : "") +
            (cyrillic != null ? "cyrillic=" + cyrillic + ", " : "") +
            (english != null ? "english=" + english + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
