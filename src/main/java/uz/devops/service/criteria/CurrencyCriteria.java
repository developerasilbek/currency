package uz.devops.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CurrencyCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter cbuId;

    private IntegerFilter code;

    private StringFilter name;

    private IntegerFilter nominal;

    private DoubleFilter rate;

    private DoubleFilter difference;

    private LocalDateFilter date;

    private LongFilter languageId;

    private Boolean distinct;

    public CurrencyCriteria() {}

    public CurrencyCriteria(CurrencyCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.cbuId = other.cbuId == null ? null : other.cbuId.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.nominal = other.nominal == null ? null : other.nominal.copy();
        this.rate = other.rate == null ? null : other.rate.copy();
        this.difference = other.difference == null ? null : other.difference.copy();
        this.date = other.date == null ? null : other.date.copy();
        this.languageId = other.languageId == null ? null : other.languageId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public CurrencyCriteria copy() {
        return new CurrencyCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getCbuId() {
        return cbuId;
    }

    public IntegerFilter cbuId() {
        if (cbuId == null) {
            cbuId = new IntegerFilter();
        }
        return cbuId;
    }

    public void setCbuId(IntegerFilter cbuId) {
        this.cbuId = cbuId;
    }

    public IntegerFilter getCode() {
        return code;
    }

    public IntegerFilter code() {
        if (code == null) {
            code = new IntegerFilter();
        }
        return code;
    }

    public void setCode(IntegerFilter code) {
        this.code = code;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public IntegerFilter getNominal() {
        return nominal;
    }

    public IntegerFilter nominal() {
        if (nominal == null) {
            nominal = new IntegerFilter();
        }
        return nominal;
    }

    public void setNominal(IntegerFilter nominal) {
        this.nominal = nominal;
    }

    public DoubleFilter getRate() {
        return rate;
    }

    public DoubleFilter rate() {
        if (rate == null) {
            rate = new DoubleFilter();
        }
        return rate;
    }

    public void setRate(DoubleFilter rate) {
        this.rate = rate;
    }

    public DoubleFilter getDifference() {
        return difference;
    }

    public DoubleFilter difference() {
        if (difference == null) {
            difference = new DoubleFilter();
        }
        return difference;
    }

    public void setDifference(DoubleFilter difference) {
        this.difference = difference;
    }

    public LocalDateFilter getDate() {
        return date;
    }

    public LocalDateFilter date() {
        if (date == null) {
            date = new LocalDateFilter();
        }
        return date;
    }

    public void setDate(LocalDateFilter date) {
        this.date = date;
    }

    public LongFilter getLanguageId() {
        return languageId;
    }

    public LongFilter languageId() {
        if (languageId == null) {
            languageId = new LongFilter();
        }
        return languageId;
    }

    public void setLanguageId(LongFilter languageId) {
        this.languageId = languageId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CurrencyCriteria that = (CurrencyCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(cbuId, that.cbuId) &&
            Objects.equals(code, that.code) &&
            Objects.equals(name, that.name) &&
            Objects.equals(nominal, that.nominal) &&
            Objects.equals(rate, that.rate) &&
            Objects.equals(difference, that.difference) &&
            Objects.equals(date, that.date) &&
            Objects.equals(languageId, that.languageId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cbuId, code, name, nominal, rate, difference, date, languageId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CurrencyCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (cbuId != null ? "cbuId=" + cbuId + ", " : "") +
            (code != null ? "code=" + code + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (nominal != null ? "nominal=" + nominal + ", " : "") +
            (rate != null ? "rate=" + rate + ", " : "") +
            (difference != null ? "difference=" + difference + ", " : "") +
            (date != null ? "date=" + date + ", " : "") +
            (languageId != null ? "languageId=" + languageId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
