package uz.devops.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.devops.domain.Currency;
import uz.devops.domain.Language;
import uz.devops.helper.CurrencyHelper;
import uz.devops.repository.CurrencyRepository;
import uz.devops.service.CurrencyService;
import uz.devops.service.LanguageService;
import uz.devops.service.dto.CurrencyDTO;
import uz.devops.service.dto.LanguageDTO;
import uz.devops.service.dto.RequestDTO;
import uz.devops.service.mapper.CurrencyMapper;

@Service
@Transactional
public class CurrencyServiceImpl implements CurrencyService {

    private final Logger log = LoggerFactory.getLogger(CurrencyServiceImpl.class);

    private final CurrencyRepository currencyRepository;

    private final CurrencyMapper currencyMapper;

    private final CurrencyHelper currencyHelper;
    private final LanguageService languageService;

    public CurrencyServiceImpl(
        CurrencyRepository currencyRepository,
        CurrencyMapper currencyMapper,
        CurrencyHelper currencyHelper,
        LanguageService languageService
    ) {
        this.currencyRepository = currencyRepository;
        this.currencyMapper = currencyMapper;
        this.currencyHelper = currencyHelper;
        this.languageService = languageService;
    }

    @Override
    public CurrencyDTO save(CurrencyDTO currencyDTO) {
        log.debug("Request to save Currency : {}", currencyDTO);
        Currency currency = currencyMapper.toEntity(currencyDTO);
        currency = currencyRepository.save(currency);
        return currencyMapper.toDto(currency);
    }

    @Override
    public List<CurrencyDTO> saveAll() {
        List<RequestDTO> currenciesFromCbu = currencyHelper.getCurrenciesFromCbu();

        List<Language> languageList = currenciesFromCbu
            .stream()
            .map(requestDTO ->
                new Language(
                    null,
                    requestDTO.getName(),
                    requestDTO.getNameRu(),
                    requestDTO.getNameUz(),
                    requestDTO.getNameUzc(),
                    requestDTO.getNameEn()
                )
            )
            .toList();

        Map<String, Language> languageMap = languageService.saveAll(languageList);

        List<Currency> currencyList = currenciesFromCbu
            .stream()
            .map(requestDTO ->
                new Currency(
                    null,
                    requestDTO.getCbuId(),
                    requestDTO.getCode(),
                    requestDTO.getName(),
                    requestDTO.getNominal(),
                    requestDTO.getRate(),
                    requestDTO.getDifference(),
                    LocalDate.parse(requestDTO.getDate(), DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                    languageMap.get(requestDTO.getName())
                )
            )
            .toList();

        currencyList = currencyRepository.saveAll(currencyList);

        return currencyMapper.toDto(currencyList);
    }

    @Override
    public CurrencyDTO update(CurrencyDTO currencyDTO) {
        log.debug("Request to update Currency : {}", currencyDTO);
        Currency currency = currencyMapper.toEntity(currencyDTO);
        currency = currencyRepository.save(currency);
        return currencyMapper.toDto(currency);
    }

    @Override
    public Optional<CurrencyDTO> partialUpdate(CurrencyDTO currencyDTO) {
        log.debug("Request to partially update Currency : {}", currencyDTO);

        return currencyRepository
            .findById(currencyDTO.getId())
            .map(existingCurrency -> {
                currencyMapper.partialUpdate(existingCurrency, currencyDTO);

                return existingCurrency;
            })
            .map(currencyRepository::save)
            .map(currencyMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CurrencyDTO> findAll() {
        log.debug("Request to get all Currencies");
        return currencyRepository.findAll().stream().map(currencyMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CurrencyDTO> findOne(Long id) {
        log.debug("Request to get Currency : {}", id);
        return currencyRepository.findById(id).map(currencyMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Currency : {}", id);
        currencyRepository.deleteById(id);
    }
}
