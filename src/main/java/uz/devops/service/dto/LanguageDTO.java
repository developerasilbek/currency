package uz.devops.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

@SuppressWarnings("common-java:DuplicatedBlocks")
public class LanguageDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String russian;

    @NotNull
    private String uzbek;

    @NotNull
    private String cyrillic;

    @NotNull
    private String english;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRussian() {
        return russian;
    }

    public void setRussian(String russian) {
        this.russian = russian;
    }

    public String getUzbek() {
        return uzbek;
    }

    public void setUzbek(String uzbek) {
        this.uzbek = uzbek;
    }

    public String getCyrillic() {
        return cyrillic;
    }

    public void setCyrillic(String cyrillic) {
        this.cyrillic = cyrillic;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LanguageDTO)) {
            return false;
        }

        LanguageDTO languageDTO = (LanguageDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, languageDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LanguageDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", russian='" + getRussian() + "'" +
            ", uzbek='" + getUzbek() + "'" +
            ", cyrillic='" + getCyrillic() + "'" +
            ", english='" + getEnglish() + "'" +
            "}";
    }
}
