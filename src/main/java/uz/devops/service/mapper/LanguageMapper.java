package uz.devops.service.mapper;

import org.mapstruct.*;
import uz.devops.domain.Language;
import uz.devops.service.dto.LanguageDTO;

@Mapper(componentModel = "spring")
public interface LanguageMapper extends EntityMapper<LanguageDTO, Language> {}
