package uz.devops.service.mapper;

import org.mapstruct.*;
import uz.devops.domain.Currency;
import uz.devops.domain.Language;
import uz.devops.service.dto.CurrencyDTO;
import uz.devops.service.dto.LanguageDTO;

@Mapper(componentModel = "spring")
public interface CurrencyMapper extends EntityMapper<CurrencyDTO, Currency> {
    CurrencyDTO toDto(Currency s);
}
