package uz.devops.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "language")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Language implements Serializable {

    public Language() {}

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "russian", nullable = false)
    private String russian;

    @NotNull
    @Column(name = "uzbek", nullable = false)
    private String uzbek;

    @NotNull
    @Column(name = "cyrillic", nullable = false)
    private String cyrillic;

    @NotNull
    @Column(name = "english", nullable = false)
    private String english;

    public Language(Long id, String name, String russian, String uzbek, String cyrillic, String english) {
        this.id = id;
        this.name = name;
        this.russian = russian;
        this.uzbek = uzbek;
        this.cyrillic = cyrillic;
        this.english = english;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Language id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Language name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRussian() {
        return this.russian;
    }

    public Language russian(String russian) {
        this.setRussian(russian);
        return this;
    }

    public void setRussian(String russian) {
        this.russian = russian;
    }

    public String getUzbek() {
        return this.uzbek;
    }

    public Language uzbek(String uzbek) {
        this.setUzbek(uzbek);
        return this;
    }

    public void setUzbek(String uzbek) {
        this.uzbek = uzbek;
    }

    public String getCyrillic() {
        return this.cyrillic;
    }

    public Language cyrillic(String cyrillic) {
        this.setCyrillic(cyrillic);
        return this;
    }

    public void setCyrillic(String cyrillic) {
        this.cyrillic = cyrillic;
    }

    public String getEnglish() {
        return this.english;
    }

    public Language english(String english) {
        this.setEnglish(english);
        return this;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Language)) {
            return false;
        }
        return id != null && id.equals(((Language) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Language{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", russian='" + getRussian() + "'" +
            ", uzbek='" + getUzbek() + "'" +
            ", cyrillic='" + getCyrillic() + "'" +
            ", english='" + getEnglish() + "'" +
            "}";
    }
}
