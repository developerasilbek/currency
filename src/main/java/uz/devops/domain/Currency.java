package uz.devops.domain;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "currency")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Currency implements Serializable {

    public Currency() {}

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "cbu_id", nullable = false)
    private Integer cbuId;

    @NotNull
    @Column(name = "code", nullable = false)
    private Integer code;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "nominal", nullable = false)
    private Integer nominal;

    @NotNull
    @Column(name = "rate", nullable = false)
    private Double rate;

    @NotNull
    @Column(name = "difference", nullable = false)
    private Double difference;

    @NotNull
    @Column(name = "date", nullable = false)
    private LocalDate date;

    @OneToOne
    @JoinColumn(unique = true)
    private Language language;

    public Currency(
        Long id,
        Integer cbuId,
        Integer code,
        String name,
        Integer nominal,
        Double rate,
        Double difference,
        LocalDate date,
        Language language
    ) {
        this.id = id;
        this.cbuId = cbuId;
        this.code = code;
        this.name = name;
        this.nominal = nominal;
        this.rate = rate;
        this.difference = difference;
        this.date = date;
        this.language = language;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Currency id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCbuId() {
        return this.cbuId;
    }

    public Currency cbuId(Integer cbuId) {
        this.setCbuId(cbuId);
        return this;
    }

    public void setCbuId(Integer cbuId) {
        this.cbuId = cbuId;
    }

    public Integer getCode() {
        return this.code;
    }

    public Currency code(Integer code) {
        this.setCode(code);
        return this;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public Currency name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNominal() {
        return this.nominal;
    }

    public Currency nominal(Integer nominal) {
        this.setNominal(nominal);
        return this;
    }

    public void setNominal(Integer nominal) {
        this.nominal = nominal;
    }

    public Double getRate() {
        return this.rate;
    }

    public Currency rate(Double rate) {
        this.setRate(rate);
        return this;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Double getDifference() {
        return this.difference;
    }

    public Currency difference(Double difference) {
        this.setDifference(difference);
        return this;
    }

    public void setDifference(Double difference) {
        this.difference = difference;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public Currency date(LocalDate date) {
        this.setDate(date);
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Language getLanguage() {
        return this.language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Currency language(Language language) {
        this.setLanguage(language);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Currency)) {
            return false;
        }
        return id != null && id.equals(((Currency) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore

    @Override
    public String toString() {
        return "Currency{" +
            "id=" + id +
            ", cbuId=" + cbuId +
            ", code=" + code +
            ", name='" + name + '\'' +
            ", nominal=" + nominal +
            ", rate=" + rate +
            ", difference=" + difference +
            ", date=" + date +
            ", language=" + language +
            '}';
    }
}
