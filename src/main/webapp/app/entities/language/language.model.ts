export interface ILanguage {
  id: number;
  name?: string | null;
  russian?: string | null;
  uzbek?: string | null;
  cyrillic?: string | null;
  english?: string | null;
}

export type NewLanguage = Omit<ILanguage, 'id'> & { id: null };
