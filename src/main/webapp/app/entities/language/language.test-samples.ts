import { ILanguage, NewLanguage } from './language.model';

export const sampleWithRequiredData: ILanguage = {
  id: 6445,
  name: 'Unbranded Granite Facilitator',
  russian: 'Argentina Sleek copying',
  uzbek: 'synthesizing transmit',
  cyrillic: 'Soap Business-focused alarm',
  english: 'TCP',
};

export const sampleWithPartialData: ILanguage = {
  id: 53137,
  name: 'Optimization',
  russian: 'Hat',
  uzbek: 'parse deposit',
  cyrillic: 'program',
  english: 'payment',
};

export const sampleWithFullData: ILanguage = {
  id: 54708,
  name: 'deposit copy Tennessee',
  russian: 'Generic primary',
  uzbek: 'Rustic Marketing',
  cyrillic: 'invoice',
  english: 'deposit Island Electronics',
};

export const sampleWithNewData: NewLanguage = {
  name: 'program capacitor',
  russian: 'IB',
  uzbek: 'SDR Berkshire system',
  cyrillic: 'navigate neural network',
  english: 'Strategist',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
