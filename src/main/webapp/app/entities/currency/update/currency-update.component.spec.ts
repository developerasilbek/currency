import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CurrencyFormService } from './currency-form.service';
import { CurrencyService } from '../service/currency.service';
import { ICurrency } from '../currency.model';
import { ILanguage } from 'app/entities/language/language.model';
import { LanguageService } from 'app/entities/language/service/language.service';

import { CurrencyUpdateComponent } from './currency-update.component';

describe('Currency Management Update Component', () => {
  let comp: CurrencyUpdateComponent;
  let fixture: ComponentFixture<CurrencyUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let currencyFormService: CurrencyFormService;
  let currencyService: CurrencyService;
  let languageService: LanguageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CurrencyUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CurrencyUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CurrencyUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    currencyFormService = TestBed.inject(CurrencyFormService);
    currencyService = TestBed.inject(CurrencyService);
    languageService = TestBed.inject(LanguageService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Language query and add missing value', () => {
      const currency: ICurrency = { id: 456 };
      const language: ILanguage = { id: 37381 };
      currency.language = language;

      const languageCollection: ILanguage[] = [{ id: 1949 }];
      jest.spyOn(languageService, 'query').mockReturnValue(of(new HttpResponse({ body: languageCollection })));
      const additionalLanguages = [language];
      const expectedCollection: ILanguage[] = [...additionalLanguages, ...languageCollection];
      jest.spyOn(languageService, 'addLanguageToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ currency });
      comp.ngOnInit();

      expect(languageService.query).toHaveBeenCalled();
      expect(languageService.addLanguageToCollectionIfMissing).toHaveBeenCalledWith(
        languageCollection,
        ...additionalLanguages.map(expect.objectContaining)
      );
      expect(comp.languagesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const currency: ICurrency = { id: 456 };
      const language: ILanguage = { id: 2459 };
      currency.language = language;

      activatedRoute.data = of({ currency });
      comp.ngOnInit();

      expect(comp.languagesSharedCollection).toContain(language);
      expect(comp.currency).toEqual(currency);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICurrency>>();
      const currency = { id: 123 };
      jest.spyOn(currencyFormService, 'getCurrency').mockReturnValue(currency);
      jest.spyOn(currencyService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ currency });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: currency }));
      saveSubject.complete();

      // THEN
      expect(currencyFormService.getCurrency).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(currencyService.update).toHaveBeenCalledWith(expect.objectContaining(currency));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICurrency>>();
      const currency = { id: 123 };
      jest.spyOn(currencyFormService, 'getCurrency').mockReturnValue({ id: null });
      jest.spyOn(currencyService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ currency: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: currency }));
      saveSubject.complete();

      // THEN
      expect(currencyFormService.getCurrency).toHaveBeenCalled();
      expect(currencyService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICurrency>>();
      const currency = { id: 123 };
      jest.spyOn(currencyService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ currency });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(currencyService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareLanguage', () => {
      it('Should forward to languageService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(languageService, 'compareLanguage');
        comp.compareLanguage(entity, entity2);
        expect(languageService.compareLanguage).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
