import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { CurrencyFormService, CurrencyFormGroup } from './currency-form.service';
import { ICurrency } from '../currency.model';
import { CurrencyService } from '../service/currency.service';
import { ILanguage } from 'app/entities/language/language.model';
import { LanguageService } from 'app/entities/language/service/language.service';

@Component({
  selector: 'jhi-currency-update',
  templateUrl: './currency-update.component.html',
})
export class CurrencyUpdateComponent implements OnInit {
  isSaving = false;
  currency: ICurrency | null = null;

  languagesSharedCollection: ILanguage[] = [];

  editForm: CurrencyFormGroup = this.currencyFormService.createCurrencyFormGroup();

  constructor(
    protected currencyService: CurrencyService,
    protected currencyFormService: CurrencyFormService,
    protected languageService: LanguageService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareLanguage = (o1: ILanguage | null, o2: ILanguage | null): boolean => this.languageService.compareLanguage(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ currency }) => {
      this.currency = currency;
      if (currency) {
        this.updateForm(currency);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const currency = this.currencyFormService.getCurrency(this.editForm);
    if (currency.id !== null) {
      this.subscribeToSaveResponse(this.currencyService.update(currency));
    } else {
      this.subscribeToSaveResponse(this.currencyService.create(currency));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICurrency>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(currency: ICurrency): void {
    this.currency = currency;
    this.currencyFormService.resetForm(this.editForm, currency);

    this.languagesSharedCollection = this.languageService.addLanguageToCollectionIfMissing<ILanguage>(
      this.languagesSharedCollection,
      currency.language
    );
  }

  protected loadRelationshipsOptions(): void {
    this.languageService
      .query()
      .pipe(map((res: HttpResponse<ILanguage[]>) => res.body ?? []))
      .pipe(
        map((languages: ILanguage[]) =>
          this.languageService.addLanguageToCollectionIfMissing<ILanguage>(languages, this.currency?.language)
        )
      )
      .subscribe((languages: ILanguage[]) => (this.languagesSharedCollection = languages));
  }
}
