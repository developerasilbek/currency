import dayjs from 'dayjs/esm';
import { ILanguage } from 'app/entities/language/language.model';

export interface ICurrency {
  id: number;
  cbuId?: number | null;
  code?: number | null;
  name?: string | null;
  nominal?: number | null;
  rate?: number | null;
  difference?: number | null;
  date?: dayjs.Dayjs | null;
  language?: Pick<ILanguage, 'id'> | null;
}

export type NewCurrency = Omit<ICurrency, 'id'> & { id: null };
