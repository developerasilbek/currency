import dayjs from 'dayjs/esm';

import { ICurrency, NewCurrency } from './currency.model';

export const sampleWithRequiredData: ICurrency = {
  id: 10224,
  cbuId: 99853,
  code: 87868,
  name: 'productivity',
  nominal: 92789,
  rate: 82423,
  difference: 65392,
  date: dayjs('2022-12-18'),
};

export const sampleWithPartialData: ICurrency = {
  id: 74650,
  cbuId: 42242,
  code: 38279,
  name: 'Cotton seamless',
  nominal: 29476,
  rate: 19153,
  difference: 17441,
  date: dayjs('2022-12-18'),
};

export const sampleWithFullData: ICurrency = {
  id: 26403,
  cbuId: 42761,
  code: 48828,
  name: 'International Handmade',
  nominal: 72149,
  rate: 95622,
  difference: 45832,
  date: dayjs('2022-12-19'),
};

export const sampleWithNewData: NewCurrency = {
  cbuId: 54047,
  code: 84272,
  name: 'redefine Connecticut International',
  nominal: 27105,
  rate: 47034,
  difference: 87490,
  date: dayjs('2022-12-18'),
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
