import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICurrency, NewCurrency } from '../currency.model';

export type PartialUpdateCurrency = Partial<ICurrency> & Pick<ICurrency, 'id'>;

type RestOf<T extends ICurrency | NewCurrency> = Omit<T, 'date'> & {
  date?: string | null;
};

export type RestCurrency = RestOf<ICurrency>;

export type NewRestCurrency = RestOf<NewCurrency>;

export type PartialUpdateRestCurrency = RestOf<PartialUpdateCurrency>;

export type EntityResponseType = HttpResponse<ICurrency>;
export type EntityArrayResponseType = HttpResponse<ICurrency[]>;

@Injectable({ providedIn: 'root' })
export class CurrencyService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/currencies');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(currency: NewCurrency): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(currency);
    return this.http
      .post<RestCurrency>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(currency: ICurrency): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(currency);
    return this.http
      .put<RestCurrency>(`${this.resourceUrl}/${this.getCurrencyIdentifier(currency)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(currency: PartialUpdateCurrency): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(currency);
    return this.http
      .patch<RestCurrency>(`${this.resourceUrl}/${this.getCurrencyIdentifier(currency)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestCurrency>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestCurrency[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getCurrencyIdentifier(currency: Pick<ICurrency, 'id'>): number {
    return currency.id;
  }

  compareCurrency(o1: Pick<ICurrency, 'id'> | null, o2: Pick<ICurrency, 'id'> | null): boolean {
    return o1 && o2 ? this.getCurrencyIdentifier(o1) === this.getCurrencyIdentifier(o2) : o1 === o2;
  }

  addCurrencyToCollectionIfMissing<Type extends Pick<ICurrency, 'id'>>(
    currencyCollection: Type[],
    ...currenciesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const currencies: Type[] = currenciesToCheck.filter(isPresent);
    if (currencies.length > 0) {
      const currencyCollectionIdentifiers = currencyCollection.map(currencyItem => this.getCurrencyIdentifier(currencyItem)!);
      const currenciesToAdd = currencies.filter(currencyItem => {
        const currencyIdentifier = this.getCurrencyIdentifier(currencyItem);
        if (currencyCollectionIdentifiers.includes(currencyIdentifier)) {
          return false;
        }
        currencyCollectionIdentifiers.push(currencyIdentifier);
        return true;
      });
      return [...currenciesToAdd, ...currencyCollection];
    }
    return currencyCollection;
  }

  protected convertDateFromClient<T extends ICurrency | NewCurrency | PartialUpdateCurrency>(currency: T): RestOf<T> {
    return {
      ...currency,
      date: currency.date?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restCurrency: RestCurrency): ICurrency {
    return {
      ...restCurrency,
      date: restCurrency.date ? dayjs(restCurrency.date) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestCurrency>): HttpResponse<ICurrency> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestCurrency[]>): HttpResponse<ICurrency[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
