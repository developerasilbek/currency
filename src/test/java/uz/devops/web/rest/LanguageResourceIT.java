package uz.devops.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import uz.devops.IntegrationTest;
import uz.devops.domain.Language;
import uz.devops.repository.LanguageRepository;
import uz.devops.service.criteria.LanguageCriteria;
import uz.devops.service.dto.LanguageDTO;
import uz.devops.service.mapper.LanguageMapper;

/**
 * Integration tests for the {@link LanguageResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class LanguageResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_RUSSIAN = "AAAAAAAAAA";
    private static final String UPDATED_RUSSIAN = "BBBBBBBBBB";

    private static final String DEFAULT_UZBEK = "AAAAAAAAAA";
    private static final String UPDATED_UZBEK = "BBBBBBBBBB";

    private static final String DEFAULT_CYRILLIC = "AAAAAAAAAA";
    private static final String UPDATED_CYRILLIC = "BBBBBBBBBB";

    private static final String DEFAULT_ENGLISH = "AAAAAAAAAA";
    private static final String UPDATED_ENGLISH = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/languages";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private LanguageRepository languageRepository;

    @Autowired
    private LanguageMapper languageMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLanguageMockMvc;

    private Language language;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Language createEntity(EntityManager em) {
        Language language = new Language()
            .name(DEFAULT_NAME)
            .russian(DEFAULT_RUSSIAN)
            .uzbek(DEFAULT_UZBEK)
            .cyrillic(DEFAULT_CYRILLIC)
            .english(DEFAULT_ENGLISH);
        return language;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Language createUpdatedEntity(EntityManager em) {
        Language language = new Language()
            .name(UPDATED_NAME)
            .russian(UPDATED_RUSSIAN)
            .uzbek(UPDATED_UZBEK)
            .cyrillic(UPDATED_CYRILLIC)
            .english(UPDATED_ENGLISH);
        return language;
    }

    @BeforeEach
    public void initTest() {
        language = createEntity(em);
    }

    @Test
    @Transactional
    void createLanguage() throws Exception {
        int databaseSizeBeforeCreate = languageRepository.findAll().size();
        // Create the Language
        LanguageDTO languageDTO = languageMapper.toDto(language);
        restLanguageMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(languageDTO)))
            .andExpect(status().isCreated());

        // Validate the Language in the database
        List<Language> languageList = languageRepository.findAll();
        assertThat(languageList).hasSize(databaseSizeBeforeCreate + 1);
        Language testLanguage = languageList.get(languageList.size() - 1);
        assertThat(testLanguage.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testLanguage.getRussian()).isEqualTo(DEFAULT_RUSSIAN);
        assertThat(testLanguage.getUzbek()).isEqualTo(DEFAULT_UZBEK);
        assertThat(testLanguage.getCyrillic()).isEqualTo(DEFAULT_CYRILLIC);
        assertThat(testLanguage.getEnglish()).isEqualTo(DEFAULT_ENGLISH);
    }

    @Test
    @Transactional
    void createLanguageWithExistingId() throws Exception {
        // Create the Language with an existing ID
        language.setId(1L);
        LanguageDTO languageDTO = languageMapper.toDto(language);

        int databaseSizeBeforeCreate = languageRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restLanguageMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(languageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Language in the database
        List<Language> languageList = languageRepository.findAll();
        assertThat(languageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = languageRepository.findAll().size();
        // set the field null
        language.setName(null);

        // Create the Language, which fails.
        LanguageDTO languageDTO = languageMapper.toDto(language);

        restLanguageMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(languageDTO)))
            .andExpect(status().isBadRequest());

        List<Language> languageList = languageRepository.findAll();
        assertThat(languageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRussianIsRequired() throws Exception {
        int databaseSizeBeforeTest = languageRepository.findAll().size();
        // set the field null
        language.setRussian(null);

        // Create the Language, which fails.
        LanguageDTO languageDTO = languageMapper.toDto(language);

        restLanguageMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(languageDTO)))
            .andExpect(status().isBadRequest());

        List<Language> languageList = languageRepository.findAll();
        assertThat(languageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUzbekIsRequired() throws Exception {
        int databaseSizeBeforeTest = languageRepository.findAll().size();
        // set the field null
        language.setUzbek(null);

        // Create the Language, which fails.
        LanguageDTO languageDTO = languageMapper.toDto(language);

        restLanguageMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(languageDTO)))
            .andExpect(status().isBadRequest());

        List<Language> languageList = languageRepository.findAll();
        assertThat(languageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCyrillicIsRequired() throws Exception {
        int databaseSizeBeforeTest = languageRepository.findAll().size();
        // set the field null
        language.setCyrillic(null);

        // Create the Language, which fails.
        LanguageDTO languageDTO = languageMapper.toDto(language);

        restLanguageMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(languageDTO)))
            .andExpect(status().isBadRequest());

        List<Language> languageList = languageRepository.findAll();
        assertThat(languageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEnglishIsRequired() throws Exception {
        int databaseSizeBeforeTest = languageRepository.findAll().size();
        // set the field null
        language.setEnglish(null);

        // Create the Language, which fails.
        LanguageDTO languageDTO = languageMapper.toDto(language);

        restLanguageMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(languageDTO)))
            .andExpect(status().isBadRequest());

        List<Language> languageList = languageRepository.findAll();
        assertThat(languageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllLanguages() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList
        restLanguageMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(language.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].russian").value(hasItem(DEFAULT_RUSSIAN)))
            .andExpect(jsonPath("$.[*].uzbek").value(hasItem(DEFAULT_UZBEK)))
            .andExpect(jsonPath("$.[*].cyrillic").value(hasItem(DEFAULT_CYRILLIC)))
            .andExpect(jsonPath("$.[*].english").value(hasItem(DEFAULT_ENGLISH)));
    }

    @Test
    @Transactional
    void getLanguage() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get the language
        restLanguageMockMvc
            .perform(get(ENTITY_API_URL_ID, language.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(language.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.russian").value(DEFAULT_RUSSIAN))
            .andExpect(jsonPath("$.uzbek").value(DEFAULT_UZBEK))
            .andExpect(jsonPath("$.cyrillic").value(DEFAULT_CYRILLIC))
            .andExpect(jsonPath("$.english").value(DEFAULT_ENGLISH));
    }

    @Test
    @Transactional
    void getLanguagesByIdFiltering() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        Long id = language.getId();

        defaultLanguageShouldBeFound("id.equals=" + id);
        defaultLanguageShouldNotBeFound("id.notEquals=" + id);

        defaultLanguageShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLanguageShouldNotBeFound("id.greaterThan=" + id);

        defaultLanguageShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLanguageShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllLanguagesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where name equals to DEFAULT_NAME
        defaultLanguageShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the languageList where name equals to UPDATED_NAME
        defaultLanguageShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllLanguagesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where name in DEFAULT_NAME or UPDATED_NAME
        defaultLanguageShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the languageList where name equals to UPDATED_NAME
        defaultLanguageShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllLanguagesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where name is not null
        defaultLanguageShouldBeFound("name.specified=true");

        // Get all the languageList where name is null
        defaultLanguageShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllLanguagesByNameContainsSomething() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where name contains DEFAULT_NAME
        defaultLanguageShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the languageList where name contains UPDATED_NAME
        defaultLanguageShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllLanguagesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where name does not contain DEFAULT_NAME
        defaultLanguageShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the languageList where name does not contain UPDATED_NAME
        defaultLanguageShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllLanguagesByRussianIsEqualToSomething() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where russian equals to DEFAULT_RUSSIAN
        defaultLanguageShouldBeFound("russian.equals=" + DEFAULT_RUSSIAN);

        // Get all the languageList where russian equals to UPDATED_RUSSIAN
        defaultLanguageShouldNotBeFound("russian.equals=" + UPDATED_RUSSIAN);
    }

    @Test
    @Transactional
    void getAllLanguagesByRussianIsInShouldWork() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where russian in DEFAULT_RUSSIAN or UPDATED_RUSSIAN
        defaultLanguageShouldBeFound("russian.in=" + DEFAULT_RUSSIAN + "," + UPDATED_RUSSIAN);

        // Get all the languageList where russian equals to UPDATED_RUSSIAN
        defaultLanguageShouldNotBeFound("russian.in=" + UPDATED_RUSSIAN);
    }

    @Test
    @Transactional
    void getAllLanguagesByRussianIsNullOrNotNull() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where russian is not null
        defaultLanguageShouldBeFound("russian.specified=true");

        // Get all the languageList where russian is null
        defaultLanguageShouldNotBeFound("russian.specified=false");
    }

    @Test
    @Transactional
    void getAllLanguagesByRussianContainsSomething() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where russian contains DEFAULT_RUSSIAN
        defaultLanguageShouldBeFound("russian.contains=" + DEFAULT_RUSSIAN);

        // Get all the languageList where russian contains UPDATED_RUSSIAN
        defaultLanguageShouldNotBeFound("russian.contains=" + UPDATED_RUSSIAN);
    }

    @Test
    @Transactional
    void getAllLanguagesByRussianNotContainsSomething() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where russian does not contain DEFAULT_RUSSIAN
        defaultLanguageShouldNotBeFound("russian.doesNotContain=" + DEFAULT_RUSSIAN);

        // Get all the languageList where russian does not contain UPDATED_RUSSIAN
        defaultLanguageShouldBeFound("russian.doesNotContain=" + UPDATED_RUSSIAN);
    }

    @Test
    @Transactional
    void getAllLanguagesByUzbekIsEqualToSomething() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where uzbek equals to DEFAULT_UZBEK
        defaultLanguageShouldBeFound("uzbek.equals=" + DEFAULT_UZBEK);

        // Get all the languageList where uzbek equals to UPDATED_UZBEK
        defaultLanguageShouldNotBeFound("uzbek.equals=" + UPDATED_UZBEK);
    }

    @Test
    @Transactional
    void getAllLanguagesByUzbekIsInShouldWork() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where uzbek in DEFAULT_UZBEK or UPDATED_UZBEK
        defaultLanguageShouldBeFound("uzbek.in=" + DEFAULT_UZBEK + "," + UPDATED_UZBEK);

        // Get all the languageList where uzbek equals to UPDATED_UZBEK
        defaultLanguageShouldNotBeFound("uzbek.in=" + UPDATED_UZBEK);
    }

    @Test
    @Transactional
    void getAllLanguagesByUzbekIsNullOrNotNull() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where uzbek is not null
        defaultLanguageShouldBeFound("uzbek.specified=true");

        // Get all the languageList where uzbek is null
        defaultLanguageShouldNotBeFound("uzbek.specified=false");
    }

    @Test
    @Transactional
    void getAllLanguagesByUzbekContainsSomething() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where uzbek contains DEFAULT_UZBEK
        defaultLanguageShouldBeFound("uzbek.contains=" + DEFAULT_UZBEK);

        // Get all the languageList where uzbek contains UPDATED_UZBEK
        defaultLanguageShouldNotBeFound("uzbek.contains=" + UPDATED_UZBEK);
    }

    @Test
    @Transactional
    void getAllLanguagesByUzbekNotContainsSomething() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where uzbek does not contain DEFAULT_UZBEK
        defaultLanguageShouldNotBeFound("uzbek.doesNotContain=" + DEFAULT_UZBEK);

        // Get all the languageList where uzbek does not contain UPDATED_UZBEK
        defaultLanguageShouldBeFound("uzbek.doesNotContain=" + UPDATED_UZBEK);
    }

    @Test
    @Transactional
    void getAllLanguagesByCyrillicIsEqualToSomething() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where cyrillic equals to DEFAULT_CYRILLIC
        defaultLanguageShouldBeFound("cyrillic.equals=" + DEFAULT_CYRILLIC);

        // Get all the languageList where cyrillic equals to UPDATED_CYRILLIC
        defaultLanguageShouldNotBeFound("cyrillic.equals=" + UPDATED_CYRILLIC);
    }

    @Test
    @Transactional
    void getAllLanguagesByCyrillicIsInShouldWork() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where cyrillic in DEFAULT_CYRILLIC or UPDATED_CYRILLIC
        defaultLanguageShouldBeFound("cyrillic.in=" + DEFAULT_CYRILLIC + "," + UPDATED_CYRILLIC);

        // Get all the languageList where cyrillic equals to UPDATED_CYRILLIC
        defaultLanguageShouldNotBeFound("cyrillic.in=" + UPDATED_CYRILLIC);
    }

    @Test
    @Transactional
    void getAllLanguagesByCyrillicIsNullOrNotNull() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where cyrillic is not null
        defaultLanguageShouldBeFound("cyrillic.specified=true");

        // Get all the languageList where cyrillic is null
        defaultLanguageShouldNotBeFound("cyrillic.specified=false");
    }

    @Test
    @Transactional
    void getAllLanguagesByCyrillicContainsSomething() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where cyrillic contains DEFAULT_CYRILLIC
        defaultLanguageShouldBeFound("cyrillic.contains=" + DEFAULT_CYRILLIC);

        // Get all the languageList where cyrillic contains UPDATED_CYRILLIC
        defaultLanguageShouldNotBeFound("cyrillic.contains=" + UPDATED_CYRILLIC);
    }

    @Test
    @Transactional
    void getAllLanguagesByCyrillicNotContainsSomething() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where cyrillic does not contain DEFAULT_CYRILLIC
        defaultLanguageShouldNotBeFound("cyrillic.doesNotContain=" + DEFAULT_CYRILLIC);

        // Get all the languageList where cyrillic does not contain UPDATED_CYRILLIC
        defaultLanguageShouldBeFound("cyrillic.doesNotContain=" + UPDATED_CYRILLIC);
    }

    @Test
    @Transactional
    void getAllLanguagesByEnglishIsEqualToSomething() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where english equals to DEFAULT_ENGLISH
        defaultLanguageShouldBeFound("english.equals=" + DEFAULT_ENGLISH);

        // Get all the languageList where english equals to UPDATED_ENGLISH
        defaultLanguageShouldNotBeFound("english.equals=" + UPDATED_ENGLISH);
    }

    @Test
    @Transactional
    void getAllLanguagesByEnglishIsInShouldWork() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where english in DEFAULT_ENGLISH or UPDATED_ENGLISH
        defaultLanguageShouldBeFound("english.in=" + DEFAULT_ENGLISH + "," + UPDATED_ENGLISH);

        // Get all the languageList where english equals to UPDATED_ENGLISH
        defaultLanguageShouldNotBeFound("english.in=" + UPDATED_ENGLISH);
    }

    @Test
    @Transactional
    void getAllLanguagesByEnglishIsNullOrNotNull() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where english is not null
        defaultLanguageShouldBeFound("english.specified=true");

        // Get all the languageList where english is null
        defaultLanguageShouldNotBeFound("english.specified=false");
    }

    @Test
    @Transactional
    void getAllLanguagesByEnglishContainsSomething() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where english contains DEFAULT_ENGLISH
        defaultLanguageShouldBeFound("english.contains=" + DEFAULT_ENGLISH);

        // Get all the languageList where english contains UPDATED_ENGLISH
        defaultLanguageShouldNotBeFound("english.contains=" + UPDATED_ENGLISH);
    }

    @Test
    @Transactional
    void getAllLanguagesByEnglishNotContainsSomething() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        // Get all the languageList where english does not contain DEFAULT_ENGLISH
        defaultLanguageShouldNotBeFound("english.doesNotContain=" + DEFAULT_ENGLISH);

        // Get all the languageList where english does not contain UPDATED_ENGLISH
        defaultLanguageShouldBeFound("english.doesNotContain=" + UPDATED_ENGLISH);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLanguageShouldBeFound(String filter) throws Exception {
        restLanguageMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(language.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].russian").value(hasItem(DEFAULT_RUSSIAN)))
            .andExpect(jsonPath("$.[*].uzbek").value(hasItem(DEFAULT_UZBEK)))
            .andExpect(jsonPath("$.[*].cyrillic").value(hasItem(DEFAULT_CYRILLIC)))
            .andExpect(jsonPath("$.[*].english").value(hasItem(DEFAULT_ENGLISH)));

        // Check, that the count call also returns 1
        restLanguageMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLanguageShouldNotBeFound(String filter) throws Exception {
        restLanguageMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLanguageMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingLanguage() throws Exception {
        // Get the language
        restLanguageMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingLanguage() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        int databaseSizeBeforeUpdate = languageRepository.findAll().size();

        // Update the language
        Language updatedLanguage = languageRepository.findById(language.getId()).get();
        // Disconnect from session so that the updates on updatedLanguage are not directly saved in db
        em.detach(updatedLanguage);
        updatedLanguage
            .name(UPDATED_NAME)
            .russian(UPDATED_RUSSIAN)
            .uzbek(UPDATED_UZBEK)
            .cyrillic(UPDATED_CYRILLIC)
            .english(UPDATED_ENGLISH);
        LanguageDTO languageDTO = languageMapper.toDto(updatedLanguage);

        restLanguageMockMvc
            .perform(
                put(ENTITY_API_URL_ID, languageDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(languageDTO))
            )
            .andExpect(status().isOk());

        // Validate the Language in the database
        List<Language> languageList = languageRepository.findAll();
        assertThat(languageList).hasSize(databaseSizeBeforeUpdate);
        Language testLanguage = languageList.get(languageList.size() - 1);
        assertThat(testLanguage.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testLanguage.getRussian()).isEqualTo(UPDATED_RUSSIAN);
        assertThat(testLanguage.getUzbek()).isEqualTo(UPDATED_UZBEK);
        assertThat(testLanguage.getCyrillic()).isEqualTo(UPDATED_CYRILLIC);
        assertThat(testLanguage.getEnglish()).isEqualTo(UPDATED_ENGLISH);
    }

    @Test
    @Transactional
    void putNonExistingLanguage() throws Exception {
        int databaseSizeBeforeUpdate = languageRepository.findAll().size();
        language.setId(count.incrementAndGet());

        // Create the Language
        LanguageDTO languageDTO = languageMapper.toDto(language);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLanguageMockMvc
            .perform(
                put(ENTITY_API_URL_ID, languageDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(languageDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Language in the database
        List<Language> languageList = languageRepository.findAll();
        assertThat(languageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchLanguage() throws Exception {
        int databaseSizeBeforeUpdate = languageRepository.findAll().size();
        language.setId(count.incrementAndGet());

        // Create the Language
        LanguageDTO languageDTO = languageMapper.toDto(language);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLanguageMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(languageDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Language in the database
        List<Language> languageList = languageRepository.findAll();
        assertThat(languageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamLanguage() throws Exception {
        int databaseSizeBeforeUpdate = languageRepository.findAll().size();
        language.setId(count.incrementAndGet());

        // Create the Language
        LanguageDTO languageDTO = languageMapper.toDto(language);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLanguageMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(languageDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Language in the database
        List<Language> languageList = languageRepository.findAll();
        assertThat(languageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateLanguageWithPatch() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        int databaseSizeBeforeUpdate = languageRepository.findAll().size();

        // Update the language using partial update
        Language partialUpdatedLanguage = new Language();
        partialUpdatedLanguage.setId(language.getId());

        partialUpdatedLanguage.name(UPDATED_NAME).uzbek(UPDATED_UZBEK).cyrillic(UPDATED_CYRILLIC).english(UPDATED_ENGLISH);

        restLanguageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLanguage.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLanguage))
            )
            .andExpect(status().isOk());

        // Validate the Language in the database
        List<Language> languageList = languageRepository.findAll();
        assertThat(languageList).hasSize(databaseSizeBeforeUpdate);
        Language testLanguage = languageList.get(languageList.size() - 1);
        assertThat(testLanguage.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testLanguage.getRussian()).isEqualTo(DEFAULT_RUSSIAN);
        assertThat(testLanguage.getUzbek()).isEqualTo(UPDATED_UZBEK);
        assertThat(testLanguage.getCyrillic()).isEqualTo(UPDATED_CYRILLIC);
        assertThat(testLanguage.getEnglish()).isEqualTo(UPDATED_ENGLISH);
    }

    @Test
    @Transactional
    void fullUpdateLanguageWithPatch() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        int databaseSizeBeforeUpdate = languageRepository.findAll().size();

        // Update the language using partial update
        Language partialUpdatedLanguage = new Language();
        partialUpdatedLanguage.setId(language.getId());

        partialUpdatedLanguage
            .name(UPDATED_NAME)
            .russian(UPDATED_RUSSIAN)
            .uzbek(UPDATED_UZBEK)
            .cyrillic(UPDATED_CYRILLIC)
            .english(UPDATED_ENGLISH);

        restLanguageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedLanguage.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedLanguage))
            )
            .andExpect(status().isOk());

        // Validate the Language in the database
        List<Language> languageList = languageRepository.findAll();
        assertThat(languageList).hasSize(databaseSizeBeforeUpdate);
        Language testLanguage = languageList.get(languageList.size() - 1);
        assertThat(testLanguage.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testLanguage.getRussian()).isEqualTo(UPDATED_RUSSIAN);
        assertThat(testLanguage.getUzbek()).isEqualTo(UPDATED_UZBEK);
        assertThat(testLanguage.getCyrillic()).isEqualTo(UPDATED_CYRILLIC);
        assertThat(testLanguage.getEnglish()).isEqualTo(UPDATED_ENGLISH);
    }

    @Test
    @Transactional
    void patchNonExistingLanguage() throws Exception {
        int databaseSizeBeforeUpdate = languageRepository.findAll().size();
        language.setId(count.incrementAndGet());

        // Create the Language
        LanguageDTO languageDTO = languageMapper.toDto(language);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLanguageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, languageDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(languageDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Language in the database
        List<Language> languageList = languageRepository.findAll();
        assertThat(languageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchLanguage() throws Exception {
        int databaseSizeBeforeUpdate = languageRepository.findAll().size();
        language.setId(count.incrementAndGet());

        // Create the Language
        LanguageDTO languageDTO = languageMapper.toDto(language);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLanguageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(languageDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Language in the database
        List<Language> languageList = languageRepository.findAll();
        assertThat(languageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamLanguage() throws Exception {
        int databaseSizeBeforeUpdate = languageRepository.findAll().size();
        language.setId(count.incrementAndGet());

        // Create the Language
        LanguageDTO languageDTO = languageMapper.toDto(language);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restLanguageMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(languageDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Language in the database
        List<Language> languageList = languageRepository.findAll();
        assertThat(languageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteLanguage() throws Exception {
        // Initialize the database
        languageRepository.saveAndFlush(language);

        int databaseSizeBeforeDelete = languageRepository.findAll().size();

        // Delete the language
        restLanguageMockMvc
            .perform(delete(ENTITY_API_URL_ID, language.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Language> languageList = languageRepository.findAll();
        assertThat(languageList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
