package uz.devops.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import uz.devops.IntegrationTest;
import uz.devops.domain.Currency;
import uz.devops.domain.Language;
import uz.devops.repository.CurrencyRepository;
import uz.devops.service.criteria.CurrencyCriteria;
import uz.devops.service.dto.CurrencyDTO;
import uz.devops.service.mapper.CurrencyMapper;

/**
 * Integration tests for the {@link CurrencyResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CurrencyResourceIT {

    private static final Integer DEFAULT_CBU_ID = 1;
    private static final Integer UPDATED_CBU_ID = 2;
    private static final Integer SMALLER_CBU_ID = 1 - 1;

    private static final Integer DEFAULT_CODE = 1;
    private static final Integer UPDATED_CODE = 2;
    private static final Integer SMALLER_CODE = 1 - 1;

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_NOMINAL = 1;
    private static final Integer UPDATED_NOMINAL = 2;
    private static final Integer SMALLER_NOMINAL = 1 - 1;

    private static final Double DEFAULT_RATE = 1D;
    private static final Double UPDATED_RATE = 2D;
    private static final Double SMALLER_RATE = 1D - 1D;

    private static final Double DEFAULT_DIFFERENCE = 1D;
    private static final Double UPDATED_DIFFERENCE = 2D;
    private static final Double SMALLER_DIFFERENCE = 1D - 1D;

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATE = LocalDate.ofEpochDay(-1L);

    private static final String ENTITY_API_URL = "/api/currencies";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private CurrencyMapper currencyMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCurrencyMockMvc;

    private Currency currency;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Currency createEntity(EntityManager em) {
        Currency currency = new Currency()
            .cbuId(DEFAULT_CBU_ID)
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME)
            .nominal(DEFAULT_NOMINAL)
            .rate(DEFAULT_RATE)
            .difference(DEFAULT_DIFFERENCE)
            .date(DEFAULT_DATE);
        return currency;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Currency createUpdatedEntity(EntityManager em) {
        Currency currency = new Currency()
            .cbuId(UPDATED_CBU_ID)
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .nominal(UPDATED_NOMINAL)
            .rate(UPDATED_RATE)
            .difference(UPDATED_DIFFERENCE)
            .date(UPDATED_DATE);
        return currency;
    }

    @BeforeEach
    public void initTest() {
        currency = createEntity(em);
    }

    @Test
    @Transactional
    void createCurrency() throws Exception {
        int databaseSizeBeforeCreate = currencyRepository.findAll().size();
        // Create the Currency
        CurrencyDTO currencyDTO = currencyMapper.toDto(currency);
        restCurrencyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(currencyDTO)))
            .andExpect(status().isCreated());

        // Validate the Currency in the database
        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeCreate + 1);
        Currency testCurrency = currencyList.get(currencyList.size() - 1);
        assertThat(testCurrency.getCbuId()).isEqualTo(DEFAULT_CBU_ID);
        assertThat(testCurrency.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCurrency.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCurrency.getNominal()).isEqualTo(DEFAULT_NOMINAL);
        assertThat(testCurrency.getRate()).isEqualTo(DEFAULT_RATE);
        assertThat(testCurrency.getDifference()).isEqualTo(DEFAULT_DIFFERENCE);
        assertThat(testCurrency.getDate()).isEqualTo(DEFAULT_DATE);
    }

    @Test
    @Transactional
    void createCurrencyWithExistingId() throws Exception {
        // Create the Currency with an existing ID
        currency.setId(1L);
        CurrencyDTO currencyDTO = currencyMapper.toDto(currency);

        int databaseSizeBeforeCreate = currencyRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCurrencyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(currencyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Currency in the database
        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkCbuIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = currencyRepository.findAll().size();
        // set the field null
        currency.setCbuId(null);

        // Create the Currency, which fails.
        CurrencyDTO currencyDTO = currencyMapper.toDto(currency);

        restCurrencyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(currencyDTO)))
            .andExpect(status().isBadRequest());

        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = currencyRepository.findAll().size();
        // set the field null
        currency.setCode(null);

        // Create the Currency, which fails.
        CurrencyDTO currencyDTO = currencyMapper.toDto(currency);

        restCurrencyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(currencyDTO)))
            .andExpect(status().isBadRequest());

        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = currencyRepository.findAll().size();
        // set the field null
        currency.setName(null);

        // Create the Currency, which fails.
        CurrencyDTO currencyDTO = currencyMapper.toDto(currency);

        restCurrencyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(currencyDTO)))
            .andExpect(status().isBadRequest());

        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkNominalIsRequired() throws Exception {
        int databaseSizeBeforeTest = currencyRepository.findAll().size();
        // set the field null
        currency.setNominal(null);

        // Create the Currency, which fails.
        CurrencyDTO currencyDTO = currencyMapper.toDto(currency);

        restCurrencyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(currencyDTO)))
            .andExpect(status().isBadRequest());

        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRateIsRequired() throws Exception {
        int databaseSizeBeforeTest = currencyRepository.findAll().size();
        // set the field null
        currency.setRate(null);

        // Create the Currency, which fails.
        CurrencyDTO currencyDTO = currencyMapper.toDto(currency);

        restCurrencyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(currencyDTO)))
            .andExpect(status().isBadRequest());

        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDifferenceIsRequired() throws Exception {
        int databaseSizeBeforeTest = currencyRepository.findAll().size();
        // set the field null
        currency.setDifference(null);

        // Create the Currency, which fails.
        CurrencyDTO currencyDTO = currencyMapper.toDto(currency);

        restCurrencyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(currencyDTO)))
            .andExpect(status().isBadRequest());

        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = currencyRepository.findAll().size();
        // set the field null
        currency.setDate(null);

        // Create the Currency, which fails.
        CurrencyDTO currencyDTO = currencyMapper.toDto(currency);

        restCurrencyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(currencyDTO)))
            .andExpect(status().isBadRequest());

        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCurrencies() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList
        restCurrencyMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(currency.getId().intValue())))
            .andExpect(jsonPath("$.[*].cbuId").value(hasItem(DEFAULT_CBU_ID)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].nominal").value(hasItem(DEFAULT_NOMINAL)))
            .andExpect(jsonPath("$.[*].rate").value(hasItem(DEFAULT_RATE.doubleValue())))
            .andExpect(jsonPath("$.[*].difference").value(hasItem(DEFAULT_DIFFERENCE.doubleValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())));
    }

    @Test
    @Transactional
    void getCurrency() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get the currency
        restCurrencyMockMvc
            .perform(get(ENTITY_API_URL_ID, currency.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(currency.getId().intValue()))
            .andExpect(jsonPath("$.cbuId").value(DEFAULT_CBU_ID))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.nominal").value(DEFAULT_NOMINAL))
            .andExpect(jsonPath("$.rate").value(DEFAULT_RATE.doubleValue()))
            .andExpect(jsonPath("$.difference").value(DEFAULT_DIFFERENCE.doubleValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()));
    }

    @Test
    @Transactional
    void getCurrenciesByIdFiltering() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        Long id = currency.getId();

        defaultCurrencyShouldBeFound("id.equals=" + id);
        defaultCurrencyShouldNotBeFound("id.notEquals=" + id);

        defaultCurrencyShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCurrencyShouldNotBeFound("id.greaterThan=" + id);

        defaultCurrencyShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCurrencyShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllCurrenciesByCbuIdIsEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where cbuId equals to DEFAULT_CBU_ID
        defaultCurrencyShouldBeFound("cbuId.equals=" + DEFAULT_CBU_ID);

        // Get all the currencyList where cbuId equals to UPDATED_CBU_ID
        defaultCurrencyShouldNotBeFound("cbuId.equals=" + UPDATED_CBU_ID);
    }

    @Test
    @Transactional
    void getAllCurrenciesByCbuIdIsInShouldWork() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where cbuId in DEFAULT_CBU_ID or UPDATED_CBU_ID
        defaultCurrencyShouldBeFound("cbuId.in=" + DEFAULT_CBU_ID + "," + UPDATED_CBU_ID);

        // Get all the currencyList where cbuId equals to UPDATED_CBU_ID
        defaultCurrencyShouldNotBeFound("cbuId.in=" + UPDATED_CBU_ID);
    }

    @Test
    @Transactional
    void getAllCurrenciesByCbuIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where cbuId is not null
        defaultCurrencyShouldBeFound("cbuId.specified=true");

        // Get all the currencyList where cbuId is null
        defaultCurrencyShouldNotBeFound("cbuId.specified=false");
    }

    @Test
    @Transactional
    void getAllCurrenciesByCbuIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where cbuId is greater than or equal to DEFAULT_CBU_ID
        defaultCurrencyShouldBeFound("cbuId.greaterThanOrEqual=" + DEFAULT_CBU_ID);

        // Get all the currencyList where cbuId is greater than or equal to UPDATED_CBU_ID
        defaultCurrencyShouldNotBeFound("cbuId.greaterThanOrEqual=" + UPDATED_CBU_ID);
    }

    @Test
    @Transactional
    void getAllCurrenciesByCbuIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where cbuId is less than or equal to DEFAULT_CBU_ID
        defaultCurrencyShouldBeFound("cbuId.lessThanOrEqual=" + DEFAULT_CBU_ID);

        // Get all the currencyList where cbuId is less than or equal to SMALLER_CBU_ID
        defaultCurrencyShouldNotBeFound("cbuId.lessThanOrEqual=" + SMALLER_CBU_ID);
    }

    @Test
    @Transactional
    void getAllCurrenciesByCbuIdIsLessThanSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where cbuId is less than DEFAULT_CBU_ID
        defaultCurrencyShouldNotBeFound("cbuId.lessThan=" + DEFAULT_CBU_ID);

        // Get all the currencyList where cbuId is less than UPDATED_CBU_ID
        defaultCurrencyShouldBeFound("cbuId.lessThan=" + UPDATED_CBU_ID);
    }

    @Test
    @Transactional
    void getAllCurrenciesByCbuIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where cbuId is greater than DEFAULT_CBU_ID
        defaultCurrencyShouldNotBeFound("cbuId.greaterThan=" + DEFAULT_CBU_ID);

        // Get all the currencyList where cbuId is greater than SMALLER_CBU_ID
        defaultCurrencyShouldBeFound("cbuId.greaterThan=" + SMALLER_CBU_ID);
    }

    @Test
    @Transactional
    void getAllCurrenciesByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where code equals to DEFAULT_CODE
        defaultCurrencyShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the currencyList where code equals to UPDATED_CODE
        defaultCurrencyShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where code in DEFAULT_CODE or UPDATED_CODE
        defaultCurrencyShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the currencyList where code equals to UPDATED_CODE
        defaultCurrencyShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where code is not null
        defaultCurrencyShouldBeFound("code.specified=true");

        // Get all the currencyList where code is null
        defaultCurrencyShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    void getAllCurrenciesByCodeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where code is greater than or equal to DEFAULT_CODE
        defaultCurrencyShouldBeFound("code.greaterThanOrEqual=" + DEFAULT_CODE);

        // Get all the currencyList where code is greater than or equal to UPDATED_CODE
        defaultCurrencyShouldNotBeFound("code.greaterThanOrEqual=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByCodeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where code is less than or equal to DEFAULT_CODE
        defaultCurrencyShouldBeFound("code.lessThanOrEqual=" + DEFAULT_CODE);

        // Get all the currencyList where code is less than or equal to SMALLER_CODE
        defaultCurrencyShouldNotBeFound("code.lessThanOrEqual=" + SMALLER_CODE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByCodeIsLessThanSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where code is less than DEFAULT_CODE
        defaultCurrencyShouldNotBeFound("code.lessThan=" + DEFAULT_CODE);

        // Get all the currencyList where code is less than UPDATED_CODE
        defaultCurrencyShouldBeFound("code.lessThan=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByCodeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where code is greater than DEFAULT_CODE
        defaultCurrencyShouldNotBeFound("code.greaterThan=" + DEFAULT_CODE);

        // Get all the currencyList where code is greater than SMALLER_CODE
        defaultCurrencyShouldBeFound("code.greaterThan=" + SMALLER_CODE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where name equals to DEFAULT_NAME
        defaultCurrencyShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the currencyList where name equals to UPDATED_NAME
        defaultCurrencyShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCurrenciesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where name in DEFAULT_NAME or UPDATED_NAME
        defaultCurrencyShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the currencyList where name equals to UPDATED_NAME
        defaultCurrencyShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCurrenciesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where name is not null
        defaultCurrencyShouldBeFound("name.specified=true");

        // Get all the currencyList where name is null
        defaultCurrencyShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllCurrenciesByNameContainsSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where name contains DEFAULT_NAME
        defaultCurrencyShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the currencyList where name contains UPDATED_NAME
        defaultCurrencyShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCurrenciesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where name does not contain DEFAULT_NAME
        defaultCurrencyShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the currencyList where name does not contain UPDATED_NAME
        defaultCurrencyShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCurrenciesByNominalIsEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where nominal equals to DEFAULT_NOMINAL
        defaultCurrencyShouldBeFound("nominal.equals=" + DEFAULT_NOMINAL);

        // Get all the currencyList where nominal equals to UPDATED_NOMINAL
        defaultCurrencyShouldNotBeFound("nominal.equals=" + UPDATED_NOMINAL);
    }

    @Test
    @Transactional
    void getAllCurrenciesByNominalIsInShouldWork() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where nominal in DEFAULT_NOMINAL or UPDATED_NOMINAL
        defaultCurrencyShouldBeFound("nominal.in=" + DEFAULT_NOMINAL + "," + UPDATED_NOMINAL);

        // Get all the currencyList where nominal equals to UPDATED_NOMINAL
        defaultCurrencyShouldNotBeFound("nominal.in=" + UPDATED_NOMINAL);
    }

    @Test
    @Transactional
    void getAllCurrenciesByNominalIsNullOrNotNull() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where nominal is not null
        defaultCurrencyShouldBeFound("nominal.specified=true");

        // Get all the currencyList where nominal is null
        defaultCurrencyShouldNotBeFound("nominal.specified=false");
    }

    @Test
    @Transactional
    void getAllCurrenciesByNominalIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where nominal is greater than or equal to DEFAULT_NOMINAL
        defaultCurrencyShouldBeFound("nominal.greaterThanOrEqual=" + DEFAULT_NOMINAL);

        // Get all the currencyList where nominal is greater than or equal to UPDATED_NOMINAL
        defaultCurrencyShouldNotBeFound("nominal.greaterThanOrEqual=" + UPDATED_NOMINAL);
    }

    @Test
    @Transactional
    void getAllCurrenciesByNominalIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where nominal is less than or equal to DEFAULT_NOMINAL
        defaultCurrencyShouldBeFound("nominal.lessThanOrEqual=" + DEFAULT_NOMINAL);

        // Get all the currencyList where nominal is less than or equal to SMALLER_NOMINAL
        defaultCurrencyShouldNotBeFound("nominal.lessThanOrEqual=" + SMALLER_NOMINAL);
    }

    @Test
    @Transactional
    void getAllCurrenciesByNominalIsLessThanSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where nominal is less than DEFAULT_NOMINAL
        defaultCurrencyShouldNotBeFound("nominal.lessThan=" + DEFAULT_NOMINAL);

        // Get all the currencyList where nominal is less than UPDATED_NOMINAL
        defaultCurrencyShouldBeFound("nominal.lessThan=" + UPDATED_NOMINAL);
    }

    @Test
    @Transactional
    void getAllCurrenciesByNominalIsGreaterThanSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where nominal is greater than DEFAULT_NOMINAL
        defaultCurrencyShouldNotBeFound("nominal.greaterThan=" + DEFAULT_NOMINAL);

        // Get all the currencyList where nominal is greater than SMALLER_NOMINAL
        defaultCurrencyShouldBeFound("nominal.greaterThan=" + SMALLER_NOMINAL);
    }

    @Test
    @Transactional
    void getAllCurrenciesByRateIsEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where rate equals to DEFAULT_RATE
        defaultCurrencyShouldBeFound("rate.equals=" + DEFAULT_RATE);

        // Get all the currencyList where rate equals to UPDATED_RATE
        defaultCurrencyShouldNotBeFound("rate.equals=" + UPDATED_RATE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByRateIsInShouldWork() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where rate in DEFAULT_RATE or UPDATED_RATE
        defaultCurrencyShouldBeFound("rate.in=" + DEFAULT_RATE + "," + UPDATED_RATE);

        // Get all the currencyList where rate equals to UPDATED_RATE
        defaultCurrencyShouldNotBeFound("rate.in=" + UPDATED_RATE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByRateIsNullOrNotNull() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where rate is not null
        defaultCurrencyShouldBeFound("rate.specified=true");

        // Get all the currencyList where rate is null
        defaultCurrencyShouldNotBeFound("rate.specified=false");
    }

    @Test
    @Transactional
    void getAllCurrenciesByRateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where rate is greater than or equal to DEFAULT_RATE
        defaultCurrencyShouldBeFound("rate.greaterThanOrEqual=" + DEFAULT_RATE);

        // Get all the currencyList where rate is greater than or equal to UPDATED_RATE
        defaultCurrencyShouldNotBeFound("rate.greaterThanOrEqual=" + UPDATED_RATE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByRateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where rate is less than or equal to DEFAULT_RATE
        defaultCurrencyShouldBeFound("rate.lessThanOrEqual=" + DEFAULT_RATE);

        // Get all the currencyList where rate is less than or equal to SMALLER_RATE
        defaultCurrencyShouldNotBeFound("rate.lessThanOrEqual=" + SMALLER_RATE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByRateIsLessThanSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where rate is less than DEFAULT_RATE
        defaultCurrencyShouldNotBeFound("rate.lessThan=" + DEFAULT_RATE);

        // Get all the currencyList where rate is less than UPDATED_RATE
        defaultCurrencyShouldBeFound("rate.lessThan=" + UPDATED_RATE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByRateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where rate is greater than DEFAULT_RATE
        defaultCurrencyShouldNotBeFound("rate.greaterThan=" + DEFAULT_RATE);

        // Get all the currencyList where rate is greater than SMALLER_RATE
        defaultCurrencyShouldBeFound("rate.greaterThan=" + SMALLER_RATE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByDifferenceIsEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where difference equals to DEFAULT_DIFFERENCE
        defaultCurrencyShouldBeFound("difference.equals=" + DEFAULT_DIFFERENCE);

        // Get all the currencyList where difference equals to UPDATED_DIFFERENCE
        defaultCurrencyShouldNotBeFound("difference.equals=" + UPDATED_DIFFERENCE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByDifferenceIsInShouldWork() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where difference in DEFAULT_DIFFERENCE or UPDATED_DIFFERENCE
        defaultCurrencyShouldBeFound("difference.in=" + DEFAULT_DIFFERENCE + "," + UPDATED_DIFFERENCE);

        // Get all the currencyList where difference equals to UPDATED_DIFFERENCE
        defaultCurrencyShouldNotBeFound("difference.in=" + UPDATED_DIFFERENCE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByDifferenceIsNullOrNotNull() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where difference is not null
        defaultCurrencyShouldBeFound("difference.specified=true");

        // Get all the currencyList where difference is null
        defaultCurrencyShouldNotBeFound("difference.specified=false");
    }

    @Test
    @Transactional
    void getAllCurrenciesByDifferenceIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where difference is greater than or equal to DEFAULT_DIFFERENCE
        defaultCurrencyShouldBeFound("difference.greaterThanOrEqual=" + DEFAULT_DIFFERENCE);

        // Get all the currencyList where difference is greater than or equal to UPDATED_DIFFERENCE
        defaultCurrencyShouldNotBeFound("difference.greaterThanOrEqual=" + UPDATED_DIFFERENCE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByDifferenceIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where difference is less than or equal to DEFAULT_DIFFERENCE
        defaultCurrencyShouldBeFound("difference.lessThanOrEqual=" + DEFAULT_DIFFERENCE);

        // Get all the currencyList where difference is less than or equal to SMALLER_DIFFERENCE
        defaultCurrencyShouldNotBeFound("difference.lessThanOrEqual=" + SMALLER_DIFFERENCE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByDifferenceIsLessThanSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where difference is less than DEFAULT_DIFFERENCE
        defaultCurrencyShouldNotBeFound("difference.lessThan=" + DEFAULT_DIFFERENCE);

        // Get all the currencyList where difference is less than UPDATED_DIFFERENCE
        defaultCurrencyShouldBeFound("difference.lessThan=" + UPDATED_DIFFERENCE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByDifferenceIsGreaterThanSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where difference is greater than DEFAULT_DIFFERENCE
        defaultCurrencyShouldNotBeFound("difference.greaterThan=" + DEFAULT_DIFFERENCE);

        // Get all the currencyList where difference is greater than SMALLER_DIFFERENCE
        defaultCurrencyShouldBeFound("difference.greaterThan=" + SMALLER_DIFFERENCE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByDateIsEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where date equals to DEFAULT_DATE
        defaultCurrencyShouldBeFound("date.equals=" + DEFAULT_DATE);

        // Get all the currencyList where date equals to UPDATED_DATE
        defaultCurrencyShouldNotBeFound("date.equals=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByDateIsInShouldWork() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where date in DEFAULT_DATE or UPDATED_DATE
        defaultCurrencyShouldBeFound("date.in=" + DEFAULT_DATE + "," + UPDATED_DATE);

        // Get all the currencyList where date equals to UPDATED_DATE
        defaultCurrencyShouldNotBeFound("date.in=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where date is not null
        defaultCurrencyShouldBeFound("date.specified=true");

        // Get all the currencyList where date is null
        defaultCurrencyShouldNotBeFound("date.specified=false");
    }

    @Test
    @Transactional
    void getAllCurrenciesByDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where date is greater than or equal to DEFAULT_DATE
        defaultCurrencyShouldBeFound("date.greaterThanOrEqual=" + DEFAULT_DATE);

        // Get all the currencyList where date is greater than or equal to UPDATED_DATE
        defaultCurrencyShouldNotBeFound("date.greaterThanOrEqual=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where date is less than or equal to DEFAULT_DATE
        defaultCurrencyShouldBeFound("date.lessThanOrEqual=" + DEFAULT_DATE);

        // Get all the currencyList where date is less than or equal to SMALLER_DATE
        defaultCurrencyShouldNotBeFound("date.lessThanOrEqual=" + SMALLER_DATE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByDateIsLessThanSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where date is less than DEFAULT_DATE
        defaultCurrencyShouldNotBeFound("date.lessThan=" + DEFAULT_DATE);

        // Get all the currencyList where date is less than UPDATED_DATE
        defaultCurrencyShouldBeFound("date.lessThan=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList where date is greater than DEFAULT_DATE
        defaultCurrencyShouldNotBeFound("date.greaterThan=" + DEFAULT_DATE);

        // Get all the currencyList where date is greater than SMALLER_DATE
        defaultCurrencyShouldBeFound("date.greaterThan=" + SMALLER_DATE);
    }

    @Test
    @Transactional
    void getAllCurrenciesByLanguageIsEqualToSomething() throws Exception {
        Language language;
        if (TestUtil.findAll(em, Language.class).isEmpty()) {
            currencyRepository.saveAndFlush(currency);
            language = LanguageResourceIT.createEntity(em);
        } else {
            language = TestUtil.findAll(em, Language.class).get(0);
        }
        em.persist(language);
        em.flush();
        currency.setLanguage(language);
        currencyRepository.saveAndFlush(currency);
        Long languageId = language.getId();

        // Get all the currencyList where language equals to languageId
        defaultCurrencyShouldBeFound("languageId.equals=" + languageId);

        // Get all the currencyList where language equals to (languageId + 1)
        defaultCurrencyShouldNotBeFound("languageId.equals=" + (languageId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCurrencyShouldBeFound(String filter) throws Exception {
        restCurrencyMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(currency.getId().intValue())))
            .andExpect(jsonPath("$.[*].cbuId").value(hasItem(DEFAULT_CBU_ID)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].nominal").value(hasItem(DEFAULT_NOMINAL)))
            .andExpect(jsonPath("$.[*].rate").value(hasItem(DEFAULT_RATE.doubleValue())))
            .andExpect(jsonPath("$.[*].difference").value(hasItem(DEFAULT_DIFFERENCE.doubleValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())));

        // Check, that the count call also returns 1
        restCurrencyMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCurrencyShouldNotBeFound(String filter) throws Exception {
        restCurrencyMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCurrencyMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCurrency() throws Exception {
        // Get the currency
        restCurrencyMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingCurrency() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        int databaseSizeBeforeUpdate = currencyRepository.findAll().size();

        // Update the currency
        Currency updatedCurrency = currencyRepository.findById(currency.getId()).get();
        // Disconnect from session so that the updates on updatedCurrency are not directly saved in db
        em.detach(updatedCurrency);
        updatedCurrency
            .cbuId(UPDATED_CBU_ID)
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .nominal(UPDATED_NOMINAL)
            .rate(UPDATED_RATE)
            .difference(UPDATED_DIFFERENCE)
            .date(UPDATED_DATE);
        CurrencyDTO currencyDTO = currencyMapper.toDto(updatedCurrency);

        restCurrencyMockMvc
            .perform(
                put(ENTITY_API_URL_ID, currencyDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(currencyDTO))
            )
            .andExpect(status().isOk());

        // Validate the Currency in the database
        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeUpdate);
        Currency testCurrency = currencyList.get(currencyList.size() - 1);
        assertThat(testCurrency.getCbuId()).isEqualTo(UPDATED_CBU_ID);
        assertThat(testCurrency.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCurrency.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCurrency.getNominal()).isEqualTo(UPDATED_NOMINAL);
        assertThat(testCurrency.getRate()).isEqualTo(UPDATED_RATE);
        assertThat(testCurrency.getDifference()).isEqualTo(UPDATED_DIFFERENCE);
        assertThat(testCurrency.getDate()).isEqualTo(UPDATED_DATE);
    }

    @Test
    @Transactional
    void putNonExistingCurrency() throws Exception {
        int databaseSizeBeforeUpdate = currencyRepository.findAll().size();
        currency.setId(count.incrementAndGet());

        // Create the Currency
        CurrencyDTO currencyDTO = currencyMapper.toDto(currency);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCurrencyMockMvc
            .perform(
                put(ENTITY_API_URL_ID, currencyDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(currencyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Currency in the database
        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCurrency() throws Exception {
        int databaseSizeBeforeUpdate = currencyRepository.findAll().size();
        currency.setId(count.incrementAndGet());

        // Create the Currency
        CurrencyDTO currencyDTO = currencyMapper.toDto(currency);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCurrencyMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(currencyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Currency in the database
        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCurrency() throws Exception {
        int databaseSizeBeforeUpdate = currencyRepository.findAll().size();
        currency.setId(count.incrementAndGet());

        // Create the Currency
        CurrencyDTO currencyDTO = currencyMapper.toDto(currency);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCurrencyMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(currencyDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Currency in the database
        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCurrencyWithPatch() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        int databaseSizeBeforeUpdate = currencyRepository.findAll().size();

        // Update the currency using partial update
        Currency partialUpdatedCurrency = new Currency();
        partialUpdatedCurrency.setId(currency.getId());

        partialUpdatedCurrency.code(UPDATED_CODE).nominal(UPDATED_NOMINAL).rate(UPDATED_RATE);

        restCurrencyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCurrency.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCurrency))
            )
            .andExpect(status().isOk());

        // Validate the Currency in the database
        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeUpdate);
        Currency testCurrency = currencyList.get(currencyList.size() - 1);
        assertThat(testCurrency.getCbuId()).isEqualTo(DEFAULT_CBU_ID);
        assertThat(testCurrency.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCurrency.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCurrency.getNominal()).isEqualTo(UPDATED_NOMINAL);
        assertThat(testCurrency.getRate()).isEqualTo(UPDATED_RATE);
        assertThat(testCurrency.getDifference()).isEqualTo(DEFAULT_DIFFERENCE);
        assertThat(testCurrency.getDate()).isEqualTo(DEFAULT_DATE);
    }

    @Test
    @Transactional
    void fullUpdateCurrencyWithPatch() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        int databaseSizeBeforeUpdate = currencyRepository.findAll().size();

        // Update the currency using partial update
        Currency partialUpdatedCurrency = new Currency();
        partialUpdatedCurrency.setId(currency.getId());

        partialUpdatedCurrency
            .cbuId(UPDATED_CBU_ID)
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .nominal(UPDATED_NOMINAL)
            .rate(UPDATED_RATE)
            .difference(UPDATED_DIFFERENCE)
            .date(UPDATED_DATE);

        restCurrencyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCurrency.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCurrency))
            )
            .andExpect(status().isOk());

        // Validate the Currency in the database
        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeUpdate);
        Currency testCurrency = currencyList.get(currencyList.size() - 1);
        assertThat(testCurrency.getCbuId()).isEqualTo(UPDATED_CBU_ID);
        assertThat(testCurrency.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCurrency.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCurrency.getNominal()).isEqualTo(UPDATED_NOMINAL);
        assertThat(testCurrency.getRate()).isEqualTo(UPDATED_RATE);
        assertThat(testCurrency.getDifference()).isEqualTo(UPDATED_DIFFERENCE);
        assertThat(testCurrency.getDate()).isEqualTo(UPDATED_DATE);
    }

    @Test
    @Transactional
    void patchNonExistingCurrency() throws Exception {
        int databaseSizeBeforeUpdate = currencyRepository.findAll().size();
        currency.setId(count.incrementAndGet());

        // Create the Currency
        CurrencyDTO currencyDTO = currencyMapper.toDto(currency);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCurrencyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, currencyDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(currencyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Currency in the database
        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCurrency() throws Exception {
        int databaseSizeBeforeUpdate = currencyRepository.findAll().size();
        currency.setId(count.incrementAndGet());

        // Create the Currency
        CurrencyDTO currencyDTO = currencyMapper.toDto(currency);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCurrencyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(currencyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Currency in the database
        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCurrency() throws Exception {
        int databaseSizeBeforeUpdate = currencyRepository.findAll().size();
        currency.setId(count.incrementAndGet());

        // Create the Currency
        CurrencyDTO currencyDTO = currencyMapper.toDto(currency);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCurrencyMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(currencyDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Currency in the database
        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCurrency() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        int databaseSizeBeforeDelete = currencyRepository.findAll().size();

        // Delete the currency
        restCurrencyMockMvc
            .perform(delete(ENTITY_API_URL_ID, currency.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
